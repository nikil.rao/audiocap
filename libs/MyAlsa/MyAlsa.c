/**
 * @file myAlsa.c
 * @author Nikil Rao (nikil@germanautolabs.com)
 * @brief 
 * 
 */

/**
 ************************************************************************
 *                            Includes
 ************************************************************************
 */
#include "MyAlsa.h"

/**
 ************************************************************************
 *                            Variables
 ************************************************************************
 */

/**
 ************************************************************************
 *                            Private Functions
 ************************************************************************
 */

/**
 ************************************************************************
 *                            Public Functions
 ************************************************************************
 */

/**
 * @brief 
 */
int snd_pcm_set_params(uint8_t *pcm,
                       uint8_t format,
                       uint8_t channels,
                       uint32_t rate,
                       uint32_t latency)
{
    printf("LOG (snd_pcm_set_params)\n");
    return 0;
}

/**
 * @brief 
 */
void snd_pcm_hw_params_alloca(uint8_t **param)
{
    printf("LOG (snd_pcm_hw_params_alloca)\n");
    return;
}

/**
 * @brief 
 */
int snd_pcm_prepare(uint8_t *pcm)
{
    printf("LOG (snd_pcm_prepare)\n");
    return 0;
}

/**
 * @brief 
 */
int snd_pcm_open(uint8_t **pcm, const char *name, 
		         uint8_t *stream, int mode)
{
    printf("LOG (snd_pcm_open)\n");
    return 0;
}

/**
 * @brief
 */
int snd_pcm_close(uint8_t *pcm)
{
    printf("LOG (snd_pcm_close)\n");
    return 0;
}

/**
 * @brief 
 */
int snd_pcm_readi(uint8_t *pcm, void *buffer, uint16_t size)
{
    printf("LOG (snd_pcm_readi)\n");
    return 0;
}

/**
 ************************************************************************
 *                            End of File
 ************************************************************************
 */