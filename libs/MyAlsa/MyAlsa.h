/**
 * @file myAlsa.h
 * @author Nikil Rao (nikil@germanautolabs.com)
 * @brief 
 * 
 */

#ifndef MY_ALSA_H
#define MY_ALSA_H

/**
 ************************************************************************
 *                            Includes
 ************************************************************************
 */
#include <pthread.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

/**
 ************************************************************************
 *                            Variables
 ************************************************************************
 */

/**
 ************************************************************************
 *                            Private Functions
 ************************************************************************
 */

/**
 ************************************************************************
 *                            Public Functions
 ************************************************************************
 */

/**
 * @brief 
 * 
 * @param pcm 
 * @param format 
 * @param channels 
 * @param rate 
 * @param latency 
 * @return int 
 */
int snd_pcm_set_params(uint8_t *pcm,
                       uint8_t format,
                       uint8_t channels,
                       uint32_t rate,
                       uint32_t latency);

/**
 * @brief 
 * 
 * @param param 
 */
void snd_pcm_hw_params_alloca(uint8_t **param);

/**
 * @brief 
 * 
 * @param pcm 
 * @return int 
 */
int snd_pcm_prepare(uint8_t *pcm);

/**
 * @brief 
 * 
 * @param pcm 
 * @param name 
 * @param stream 
 * @param mode 
 * @return int 
 */
int snd_pcm_open(uint8_t **pcm, const char *name, 
		         uint8_t *stream, int mode);
/**
 * @brief 
 * 
 * @param pcm 
 * @return int 
 */
int snd_pcm_close(uint8_t *pcm);

/**
 * @brief 
 * 
 * @param pcm 
 * @param buffer 
 * @param size 
 * @return int 
 */
int snd_pcm_readi(uint8_t *pcm, void *buffer, uint16_t size);

#endif /* MY_ALSA_H */
/**
 ************************************************************************
 *                            End of File
 ************************************************************************
 */

