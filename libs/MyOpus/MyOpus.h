/**
 * @file   myOpus.h
 * @author Nikil Rao 
 * @brief 
 * @date   29-Jan-2019
 * 
 */

#ifndef MY_OPUS_H
#define MY_OPUS_H
/**
 ************************************************************************
 *                            Includes
 ************************************************************************
 */
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
/**
 ************************************************************************
 *                            Variables
 ************************************************************************
 */

/**
 ************************************************************************
 *                            Private Functions
 ************************************************************************
 */


/**
 ************************************************************************
 *                            Public Functions
 ************************************************************************
 */

/**
  * @brief  Get required
 *  @param  opusConfig: opus configurations
  * @retval output - memory size
  */
uint32_t MyOpus_getMemorySize(uint8_t *opusConfig);

/**
 * @brief  Init opus library
 * @param  opusConfig: opus configurations
 * @param  error: error code
 * @retval int: 0 for success
 */
int MyOpus_init(uint8_t *opusConfig, int *error);

/**
 * @brief  Init opus library
 * @param  inputBuffer 
 * @param  outputBuffer
 * @retval output - outbut buffer length.
 */
uint16_t MyOpus_encode(uint8_t *inputBuffer, uint8_t *outputBuffer);

/**
 * @brief  Deinit opus encoder and free memory
 * @param
 * @retval int: 0 for success
 */
int MyOpus_deinit(void);

#endif /* MY_OPUS_H */
/**
 ************************************************************************
 *                            End of File
 ************************************************************************
 */