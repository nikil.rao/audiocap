/**
 * @file   myOpus.c
 * @author Nikil Rao 
 * @brief 
 * @date   29-Jan-2019
 * 
 */

/**
 ************************************************************************
 *                            Includes
 ************************************************************************
 */

#include "MyOpus.h"

#define AUDIO_ENC_MAX_SIZE  48
/**
 ************************************************************************
 *                            Variables
 ************************************************************************
 */

/**
 ************************************************************************
 *                            Private Functions
 ************************************************************************
 */


/**
 ************************************************************************
 *                            Public Functions
 ************************************************************************
 */

/**
  * @brief  Get required memory size
  */
uint32_t MyOpus_getMemorySize(uint8_t *opusConfig)
{
    printf("LOG (MyOpus_getMemorySize))\n");
    return 1;
}

/**
 * @brief  Init opus library
 */
int MyOpus_init(uint8_t *opusConfig, int *error)
{
    printf("LOG (MyOpus_init)\n");
    return 0;
}

/**
 * @brief  Init opus library
 */
uint16_t MyOpus_encode(uint8_t *inputBuffer, uint8_t *outputBuffer)
{
    printf("LOG (MyOpus_encode)\n");

    //Fill output with input+i for testing
    for (uint8_t i=0;i<AUDIO_ENC_MAX_SIZE;i++) 
    {
        outputBuffer[i] = inputBuffer[i] + i;
    }

    return AUDIO_ENC_MAX_SIZE;
}

/**
 * @brief  Deinit opus encoder and free memory
 */
int MyOpus_deinit(void)
{
    printf("LOG (MyOpus_deinit)\n");
    return 0;
}