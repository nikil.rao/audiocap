/**
 * @file   opus.h
 * @author Nikil Rao 
 * @brief 
 * @date   29-Jan-2019
 * 
 */

#ifndef OPUS_H
#define OPUS_H
/**
 ************************************************************************
 *                            Includes
 ************************************************************************
 */

#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include "defines.h"
#include "audio.h"
#include "MyOpus.h"

/**
 ************************************************************************
 *                            Variables
 ************************************************************************
 */

/**
 * @brief enum for the status error codes
 * 
 */

/**
 ************************************************************************
 *                            Private Functions
 ************************************************************************
 */


/**
 ************************************************************************
 *                            Public Functions
 ************************************************************************
 */

/**
  * @brief  Initialize opus library
  * @param  None
  * @retval STATUS_t: STATUS_OK if the configuration is ok, 
  *         STATUS_ERROR otherwise.
  */
status_t opus_init(void);

/**
 * @brief  This function is called to encode, encapsulate and save audio data.
 * @param  input_buffer: data to be encoded
 * @retval STATUS_t: Value indicating success or error code.
 */
status_t opus_encodeAudio(uint8_t *input_buffer);

/**
 * @brief  Deinit opus encoder and free memory
 * @param
 * @retval STATUS_t: Value indicating success or error code.
 */
status_t opus_deinit(void);

#endif /* OPUS_H */
/**
 ************************************************************************
 *                            End of File
 ************************************************************************
 */