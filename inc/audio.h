/**
 * @file audio.h
 * @author Nikil Rao (nikil@germanautolabs.com)
 * @brief 
 * 
 */

#ifndef AUDIO_H
#define AUDIO_H

/**
 ************************************************************************
 *                            Includes
 ************************************************************************
 */
#include <pthread.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include "defines.h"
#include "MyAlsa.h"

/**
 ************************************************************************
 *                            Variables
 ************************************************************************
 */

//Dummy values
#define AUDIO_NUMBER_OF_CHANNELS      2         //Stereo
#define AUDIO_FREQ                    16000     //16KHz
#define AUDIO_FORMAT                  2         //Signed 16 bit Little Endian = 2 Bytes
#define AUDIO_REC_CHUNK_DURATION_MS   10        //10 ms
#define AUDIO_REC_CHUNK_SIZE_BYTES    10
#define AUDIO_ENC_MAX_SIZE            AUDIO_REC_CHUNK_SIZE_BYTES
#define AUDIO_PROCESSING_TIME_MS      1000
#define AUDIO_ENCODING_TIME_MS        1

/**
 ************************************************************************
 *                            Private Functions
 ************************************************************************
 */

/**
 ************************************************************************
 *                            Public Functions
 ************************************************************************
 */

/**
 * @brief initialize the audio input device
 * 
 * @param cfg input audio device configuration
 * 
 * @return status_t STATUS_OK if success, error code otherwiseaudio error/success code 
 */
status_t audio_initInputDevice(void);

/**
 * @brief deinitialize the audio input device
 * 
 * @return status_t STATUS_OK if success, error code otherwiseaudio error/success code
 */
status_t audio_deinitInputDevice(void);

/**
 * @brief connect the audio input device before streaming audio
 * 
 * @return status_t STATUS_OK if success, error code otherwise
 */
status_t audio_connectInputDevice(void);

/**
 * @brief disconnect the audio input device
 * 
 * @return status_t STATUS_OK if success, error code otherwise
 */
status_t audio_disconnectInputDevice(void);

/**
 * @brief get a specified duration of the audio input stream 
 * 
 * @param buffer pointer to the buffer containing the input audio stream data
 * 
 * @return status_t STATUS_OK if success, error code otherwise
 */
status_t audio_getInputAudioChunk(void* buffer);

#endif /* AUDIO_H */
/**
 ************************************************************************
 *                            End of File
 ************************************************************************
 */