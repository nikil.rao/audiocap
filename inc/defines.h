/**
 * @file   defines.h
 * @author Nikil Rao 
 * @brief 
 * @date   29-Jan-2019
 * 
 */
#ifndef DEFINES_H
#define DEFINES_H

#define SAVE_ENCODED_AUDIO_TO_FILE
// #define TESTING_ENABLED
/**
 ************************************************************************
 *                            Includes
 ************************************************************************
 */
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
/**
 ************************************************************************
 *                            Variables
 ************************************************************************
 */
typedef enum
{
    STATUS_OK = 0,
    STATUS_ERROR = 1,
    STATUS_BUSY = 2,
    STATUS_REPEAT = 3,
}status_t;

#endif /* DEFINES_H */
/**
 ************************************************************************
 *                            End of File
 ************************************************************************
 */