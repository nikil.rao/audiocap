# use the official gcc image, based on debian
# can use verions as well, like gcc:5.2
# see https://hub.docker.com/_/gcc/
image: gcc

stages:
  - fetch-version
  - build
  - release

fetch-semantic-version:
  # Requires Node >= 10.13 version
  image: node:13.8.0
  stage: fetch-version
  only:
    refs:
    - master
    - /^(([0-9]+)\.)?([0-9]+)\.x/ # This matches maintenance branches
  script:
    - npm ci
    - npx semantic-release --generate-notes false --dry-run
  artifacts:
    paths:
    - VERSION.txt

generate-non-semantic-version:
  stage: fetch-version
  except:
    refs:
    - master
    - alpha
    - /^(([0-9]+)\.)?([0-9]+)\.x/ # This matches maintenance branches
    - /^([0-9]+)\.([0-9]+)\.([0-9]+)(?:-([0-9A-Za-z-]+(?:\.[0-9A-Za-z-]+)*))?(?:\+[0-9A-Za-z-]+)?$/ # This matches pre-releases
  script:
    - git describe --long --dirty --tags > VERSION.txt
  artifacts:
    paths:
    - VERSION.txt

build:
  stage: build
  # instead of calling g++ directly you can also use some build toolkit like make
  # install the necessary build tools when needed
  before_script:
    - apt update && apt -y install cmake
  script:
    - NEXT_VERSION=`cat VERSION.txt`
    - echo $NEXT_VERSION
    - mkdir build
    - cd build
    - cmake ..
    - make
    - mv audiocap audiocap-$NEXT_VERSION
  artifacts:
    paths:
      - build/audiocap*
      - VERSION.txt

release:
  image: node:13.8.0
  stage: release
  only:
    refs:
    - master
    # This matches maintenance branches
    - /^(([0-9]+)\.)?([0-9]+)\.x/
  script:
    - |
      NEXT_VERSION=`cat VERSION.txt`
      echo $NEXT_VERSION
      apt -y install curl
      RELEASE_ZIP_FILE="${NEXT_VERSION}.tar.gz"
      tar -czvf ${RELEASE_ZIP_FILE} build/*
      curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ${RELEASE_ZIP_FILE} ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/audiocap/${NEXT_VERSION}/${RELEASE_ZIP_FILE}
      npm ci
      npx semantic-release
  artifacts:
    paths:
    - build/audiocap*
    - CHANGELOG.md
