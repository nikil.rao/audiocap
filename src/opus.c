/**
 * @file   myOpus.c
 * @author Nikil Rao 
 * @brief 
 * @date   29-Jan-2019
 * 
 */

/**
 ************************************************************************
 *                            Includes
 ************************************************************************
 */

#include "opus.h"

static uint8_t opus_output_buffer[AUDIO_ENC_MAX_SIZE];
/**
 ************************************************************************
 *                            Variables
 ************************************************************************
 */
static uint8_t opus_config;

/**
 ************************************************************************
 *                            Private Functions
 ************************************************************************
 */
int saveAudioToFile(uint8_t *output_buffer, uint16_t output_buffer_len)
{
   status_t status = STATUS_OK;
   printf("LOG (Saving encoded audio to file\n");

   return status;
}

/**
 ************************************************************************
 *                            Public Functions
 ************************************************************************
 */

/**
  * @brief  Initialize opus library
  */
status_t opus_init(void)
{
    status_t status = STATUS_OK;
    int error_code;

    uint32_t enc_size = MyOpus_getMemorySize(&opus_config);

    status = MyOpus_init(&opus_config, &error_code);

    printf("LOG (opus_init): MyOpus library initialized successfully\n");
    return status;
}

/**
 * @brief  This function is called to encode, encapsulate and save audio data.
 */
status_t opus_encodeAudio(uint8_t *input_buffer)
{
   status_t status = STATUS_OK;
   uint16_t output_buffer_len = 0;

   /* Encapsulate data*/
   output_buffer_len = MyOpus_encode(input_buffer, 
                                     opus_output_buffer);

#ifdef TESTING_ENABLED
   for (uint8_t i=0;i<AUDIO_ENC_MAX_SIZE;i++) 
   {
      printf("LOG (opus_encodeAudio): Encoded buffer value  %d\n", opus_output_buffer[i]);
   }
#endif

   if(output_buffer_len <= 0)
   {
      return STATUS_ERROR;
   }

#ifdef SAVE_ENCODED_AUDIO_TO_FILE
   status = saveAudioToFile(opus_output_buffer, output_buffer_len);
   if(status != STATUS_OK)
   {
      printf("ERROR (opus_encodeAudio):Error saving audio to file");
      return STATUS_ERROR;
   }
#endif
   //Simulate encoding time
   usleep(AUDIO_ENCODING_TIME_MS * 1000);
   
   return status;
}

/**
 * @brief  Deinit opus encoder and free memory
 */
status_t opus_deinit(void)
{
   printf("LOG (opus_deinit): MyOpus library deinitialized successfully\n");
   return STATUS_OK;
}