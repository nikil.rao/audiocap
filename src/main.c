/**
 * @file   main.c
 * @author Nikil Rao 
 * @brief  main application
 * @date   29-Jan-2019
 * 
 */

/**
 ************************************************************************
 *                            Includes
 ************************************************************************
 */

#include "main.h"

/**
 ************************************************************************
 *                            Variables
 ************************************************************************
 */

// Declaration of thread condition variable 
pthread_cond_t cond1 = PTHREAD_COND_INITIALIZER; 
  
// declaring mutex 
pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER; 

//double buffers for recording mic audio chunks
static uint8_t aud_rec_buffer_1[AUDIO_REC_CHUNK_SIZE_BYTES];
static uint8_t aud_rec_buffer_2[AUDIO_REC_CHUNK_SIZE_BYTES];

//Status of buffers which is filled currently
typedef enum
{
    FILL_STATE_NONE = 0,
    FILL_STATE_BUFFER_1 = 1,
    FILL_STATE_BUFFER_2 = 2
}audio_buffer_fill_state_t;

uint32_t pending_audio_frames = 0;

/**
 ************************************************************************
 *                            Functions
 ************************************************************************
 */

/**
 * @brief thread to receive an audio chunk of predefined size and pass it
 * to the MyOpus encoder where it is encoded and then stored in a file
 * 
 * @param data 
 * @return void* 
 */
void *opusthread(void *data)
{
    uint8_t *buffer_address = (uint8_t*)data;

    if (opus_encodeAudio(buffer_address) != STATUS_OK)
    {
        printf("ERROR (opusthread): Error encoding audio. Exiting...\n");
        exit(0);
    }
    
    pthread_mutex_lock(&lock);
    pending_audio_frames--;
    printf("LOG (opusthread): Encoding done, frames to process - %d\n", pending_audio_frames);
    pthread_mutex_unlock(&lock);
}

/**
 * @brief Thread which continuously records live microphone audio data in chunks.
 * The chunk size is predefined. The data is recorded into two buffers alternatively.
 * Once data is recorded into the first buffer, a thread is created to encode this 
 * audio chunk in Opus format. While the thread is encoding the first buffer we 
 * continue with recording audio data to the second buffer. Once this is completed,
 * a thread is created to encode this data and audio recording continues into the 
 * first buffer and so on. The assumption made is that the encoding of the first 
 * chunk of audio is completed before the second chunk of audio is completely 
 * recorded. If it isn't the function throws an exception and exits. The way this
 * is tracked is by incrementing a counter for every data frame recorded and 
 * decrementing the counter when it is encoded. Therefore the max value of this 
 * counter is 1 meaning that data is encoded before the next chunk of data is recorded
 * 
 * @param data thread argument
 * @return void* return value
 */
void *audiothread(void *data)
{
    pthread_t opus_thread;
    static audio_buffer_fill_state_t state;
    uint8_t *buffer_address = NULL;
    
    for (;;) 
    {
        //If recording hasn't started or if 1st buffer needs to be filled
        if(state == FILL_STATE_BUFFER_1 || state == FILL_STATE_NONE)
        {
            printf("LOG (audiothread): Write to 1st buffer\n");
            buffer_address = aud_rec_buffer_1;
            state = FILL_STATE_BUFFER_2;        
        }
        else if (state == FILL_STATE_BUFFER_2)
        {
            printf("LOG (audiothread): Write to 2nd buffer\n");
            buffer_address = aud_rec_buffer_2;
            state = FILL_STATE_BUFFER_1;
        }
        
        if (audio_getInputAudioChunk(buffer_address) != STATUS_OK)
        {
            printf("ERROR (audiothread): Error getting mic audio. Exiting...\n");
            pthread_exit(NULL);
        }

#ifdef TESTING_ENABLED
        for (uint8_t i=0;i<AUDIO_REC_CHUNK_SIZE_BYTES;i++) 
        {
            printf("LOG (audiothread): Audio buffer value %d\n", buffer_address[i]);
        }
#endif

        //Lock variable so that the other thread doesn't change it
        pthread_mutex_lock(&lock);
        pending_audio_frames++;
        printf("LOG (audiothread): Recording done, frames to process - %d\n", pending_audio_frames);

        if(pending_audio_frames > 1)
        {
            printf("ERROR (audiothread): Encoding audio data could not be completed" 
                    " before next chunk of audio came in. Exiting...\n");
            pthread_exit(NULL);
        }

        pthread_mutex_unlock(&lock);

        //Create thread to encode audio chunk from the right buffer
        pthread_create(&opus_thread, NULL, opusthread, buffer_address); 
    }    
}

/**
 * @brief main loop. Initilaizes the required modules and starts a thread 
 * to record audio data from the microphone continuously
 * 
 * @param argc arg count
 * @param argv CLI arguments
 * @return int return code
 */
int main(int argc, char **argv) 
{
    int record_duration = 0;
    pthread_t audio_thread;

    printf("---------------------------------audiocap--------------------------------------\n");
    printf("Application to capture mic audio, encode it in opus format and store in a file\n\n");
    
    audio_initInputDevice();
    audio_connectInputDevice();
    opus_init();

    pthread_create(&audio_thread, NULL, audiothread, NULL);
    pthread_join(audio_thread, NULL);

    printf("\n");
    opus_deinit();
    audio_disconnectInputDevice();
    audio_deinitInputDevice();

    return 0;
}
