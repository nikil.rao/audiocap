/**
 * @file myAlsa.c
 * @author Nikil Rao (nikil@germanautolabs.com)
 * @brief 
 * 
 */

/**
 ************************************************************************
 *                            Includes
 ************************************************************************
 */
#include "audio.h"

/**
 ************************************************************************
 *                            Variables
 ************************************************************************
 */

static uint8_t *pcm_capture_handle = NULL;
static uint8_t *pcm_capture_params = NULL;
/**
 ************************************************************************
 *                            Private Functions
 ************************************************************************
 */

/**
 ************************************************************************
 *                            Public Functions
 ************************************************************************
 */

/**
 * @brief initialize the audio input device
 */
status_t audio_initInputDevice(void)
{
    status_t status = STATUS_OK;

	/* Allocate parameters object and fill it with default values*/
	snd_pcm_hw_params_alloca(&pcm_capture_params);

	if ((status = snd_pcm_set_params(pcm_capture_handle,
                                     AUDIO_FORMAT,
	                                 AUDIO_NUMBER_OF_CHANNELS,
	                                 AUDIO_FREQ,
	                                 500000))  != STATUS_OK) 
    {
        return STATUS_ERROR;
	}

    if ((status = snd_pcm_prepare (pcm_capture_handle))  != STATUS_OK) 
    {
        return STATUS_ERROR;
    }

    printf("LOG (audio_initInputDevice): Audio input device initialized successfully\n");

    return status;
}

/**
 * @brief deinitialize the audio input device
 */
status_t audio_deinitInputDevice(void)
{
    status_t status = STATUS_OK;
    
    pcm_capture_params = NULL;
    
    if(pcm_capture_handle == NULL)
    {
        return STATUS_ERROR;
    }

    printf("LOG (audio_deinitInputDevice): Audio input device deinitialized successfully\n");
    return status;
}

/**
 * @brief connect the audio input device before streaming audio
 */
status_t audio_connectInputDevice(void)
{
    status_t status = STATUS_OK;

    uint8_t *stream_capture = NULL;

    if ((status = snd_pcm_open (&pcm_capture_handle, 
                                "default", 
                                stream_capture, 
                                0)) != STATUS_OK) 
    {

        return STATUS_ERROR;
    }
    printf("LOG (audio_connectInputDevice): Audio input device connected\n"); 
    return status;
}

/**
 * @brief disconnect the audio input device
 */
status_t audio_disconnectInputDevice(void)
{
    status_t status = STATUS_OK;

    if ((status = snd_pcm_close(pcm_capture_handle)) 
                    != STATUS_OK)
    {
        return STATUS_ERROR;
    }

    pcm_capture_handle = NULL;

    printf("LOG (audio_disconnectInputDevice): Audio input device disconnected\n"); 
    return status;
}

/**
 * @brief get a specified duration of the audio input stream 
 */
status_t audio_getInputAudioChunk(void* buffer)
{
    status_t status = STATUS_OK;

#ifdef TESTING_ENABLED
    static uint8_t cnt = 0;
    //Increase value of dummy test data
    memset(buffer, cnt++, AUDIO_REC_CHUNK_SIZE_BYTES*2);
    usleep(AUDIO_PROCESSING_TIME_MS * 1000);
#else
    do 
    {
        status = snd_pcm_readi(pcm_capture_handle, 
                              (uint8_t*)buffer, 
                              AUDIO_REC_CHUNK_SIZE_BYTES);
    } while (status == STATUS_REPEAT);

    //Simulate audio recording time
    usleep(AUDIO_PROCESSING_TIME_MS * 1000);
#endif

    return status;
}

/**
 ************************************************************************
 *                            End of File
 ************************************************************************
 */