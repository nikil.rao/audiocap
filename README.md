# AUDIOCAP APPLICATION
 Application to capture mic audio, encode it in opus format and store in a file

 The application uses two threads. The first thread continuously records live microphone audio data in chunks. The chunk size is predefined. The data is recorded into two buffers alternatively.Once data is recorded into the first buffer, a thread is created to encode this audio chunk in Opus format. While the thread is encoding the first buffer we continue with recording audio data to the second buffer. Once this is completed,a thread is created to encode this data and audio recording continues into the first buffer and so on. The assumption made is that the encoding of the first chunk of audio is completed before the second chunk of audio is completely recorded. If it isn't the function throws an exception and exits. The way thisis tracked is by incrementing a counter for every data frame recorded and decrementing the counter when it is encoded. Therefore the max value of this counter is 1 meaning that data is encoded before the next chunk of data is recorded. To exit application press ctrl+c.

## Steps to build
1. In the main project directory, create build folder - mkdir build
2. cd build
3. cmake ..
4. make clean
5. make
6. ./audiocap

## Testing
Logs are pritned from each function to show the flow
If #define TESTING_ENABLED is uncommented in defines.h then the flow can also be tested with dummy data.
In this case the chunk size is 10 bytes. Each byte in the audio data frame is filled with the same value, which keeps increasing every time an audio frame is fetched. 
The encoded value has this formula - 
outputBuffer[i] = inputBuffer[i] + i, where i goes from 0-9 since chunk size is 10

